Rails.application.routes.draw do
  root 'comments#index' # ここを追記します
  get 'comments/index'
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
